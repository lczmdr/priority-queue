package org.queue.priority;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Priorities {

    public List<Student> getStudents(List<String> events) {

        PriorityQueue<Student> priorityQueue = new PriorityQueue<>(events.size());

        for (String event : events) {
            String[] fields = event.split("\\s");
            String command = fields[0];
            if (command.startsWith("ENTER")) {
                priorityQueue.add(new Student(fields[1], Double.parseDouble(fields[2]), Integer.parseInt(fields[3])));
            }
            else if (command.startsWith("SERVED")) {
                priorityQueue.poll();
            }
        }
        List<Student> remaining = new ArrayList<>();
        while (!priorityQueue.isEmpty()) {
            remaining.add(priorityQueue.poll());
        }
        return remaining;
    }
}

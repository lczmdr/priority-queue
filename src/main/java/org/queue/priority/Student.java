package org.queue.priority;

public class Student implements Comparable<Student> {

    private String name;

    private double cgpa;

    private int id;

    public Student(String name, double cgpa, int id) {
        this.name = name;
        this.cgpa = cgpa;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public double getCgpa() {
        return cgpa;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(Student student) {
        if (student==null) {
            return 1;
        }
        if (cgpa == student.cgpa) {
            if (name.compareTo(student.getName())==0) {
                if (id==student.getId())
                    return 0;
                return id > student.getId() ? 1 : -1;
            }
            return name.compareTo(student.getName());
        }
        return cgpa > student.cgpa ? -1 : 1;
    }
}

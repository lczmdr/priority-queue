import org.junit.jupiter.api.Test;
import org.queue.priority.Priorities;
import org.queue.priority.Student;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PrioritiesTest {

    private Priorities priorities = new Priorities();

    @Test
    public void defaultResultTest() {
        List<String> events = new ArrayList<>();
        events.add("ENTER John 3.75 50");
        events.add("ENTER Mark 3.8 24");
        events.add("ENTER Shafaet 3.7 35");
        events.add("SERVED");
        events.add("SERVED");
        events.add("ENTER Samiha 3.85 36");
        events.add("SERVED");
        events.add("ENTER Ashley 3.9 42");
        events.add("ENTER Maria 3.6 46");
        events.add("ENTER Anik 3.95 49");
        events.add("ENTER Dan 3.95 50");
        events.add("SERVED");
        List<Student> students = priorities.getStudents(events);
        assertNotNull(students);
        assertFalse(students.isEmpty());
        assertEquals(4, students.size());
        assertEquals("Dan", students.get(0).getName());
        assertEquals("Ashley", students.get(1).getName());
        assertEquals("Shafaet", students.get(2).getName());
        assertEquals("Maria", students.get(3).getName());
        printStudents(students);
    }

    @Test
    public void emptyResultTest() {
        List<String> events = new ArrayList<>();
        events.add("ENTER John 3.75 50");
        events.add("ENTER Mark 3.8 24");
        events.add("ENTER Shafaet 3.7 35");
        events.add("SERVED");
        events.add("SERVED");
        events.add("ENTER Samiha 3.85 36");
        events.add("SERVED");
        events.add("ENTER Ashley 3.9 42");
        events.add("ENTER Maria 3.6 46");
        events.add("ENTER Anik 3.95 49");
        events.add("ENTER Dan 3.95 50");
        events.add("SERVED");
        events.add("SERVED");
        events.add("SERVED");
        events.add("SERVED");
        events.add("SERVED");
        List<Student> students = priorities.getStudents(events);
        assertNotNull(students);
        assertTrue(students.isEmpty());
        printStudents(students);
    }

    @Test
    public void invalidLineTest() {
        List<String> events = new ArrayList<>();
        events.add("ENTER John 3.75 50");
        events.add("ENTER Mark 3.8 24");
        events.add("ENTER Shafaet 3.7 35");
        events.add("SERV");
        events.add("SERVED");
        events.add("ENTER Samiha 3.85 36");
        events.add("SERVED");
        events.add("ENTER Ashley 3.9 42");
        events.add("ENTER Maria 3.6 46");
        events.add("ENTER Anik 3.95 49");
        events.add("ENTER Dan 3.95 50");
        events.add("SERVED");
        List<Student> students = priorities.getStudents(events);
        assertNotNull(students);
        assertFalse(students.isEmpty());
        assertEquals(5, students.size());
        assertEquals("Dan", students.get(0).getName());
        assertEquals("Ashley", students.get(1).getName());
        assertEquals("John", students.get(2).getName());
        assertEquals("Shafaet", students.get(3).getName());
        assertEquals("Maria", students.get(4).getName());
        printStudents(students);
    }

    private void printStudents(List<Student> students) {
        if (students.isEmpty()) {
            System.out.println("EMPTY");
        }
        for (Student student : students) {
            System.out.println(student.getName());
        }
    }

}
